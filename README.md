# Opencart twig helper

This package provides some macros and functions to help you work with twig templates in Opencart 3.x

## Installation

```bash
composer require twom/twig-helper
```

### For twig 3.x:
Add to `system/library/template/twig.php`

```php
$loader->addPath(DIR_VENDOR . 'upline/opencart-twig-helper/src/twig/');
```


## Usage 


```twig
{% import "twig_helper_forms.twig" as forms %}


{# Render form field wrapper #}
{{ forms.field('field_name_that_used_for_label_for_attribute_and_input', rendered_element_form, label_usually_entry_something) }}

{# Render element form #}
{{ forms.input('field_name_that_used_for_label_for_attribute_and_input', value, {required: true}) }}
```