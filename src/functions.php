<?php

namespace Upline\OpencartTwigHelper;

function prepareItemsForForm(array $items, string $id, string $label = 'name'): array
{
    return array_map(function ($item) use ($id, $label) {
        return [
            'id' => $item[$id],
            'name' => $item[$label]
        ];
    }, $items);
}